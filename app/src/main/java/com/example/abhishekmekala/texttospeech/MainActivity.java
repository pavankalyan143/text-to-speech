package com.example.abhishekmekala.texttospeech;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private TextToSpeech mtext;
    private EditText medit;
    private SeekBar mpitch,msound;
    private Button msay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msay=findViewById(R.id.button);
        mtext =new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status==TextToSpeech.SUCCESS)
                {
                    int ans=mtext.setLanguage(Locale.ENGLISH);
                    if(ans==TextToSpeech.LANG_MISSING_DATA||ans==TextToSpeech.LANG_NOT_SUPPORTED)
                    {
                        Log.e("TTS", "Language Not Supported");
                    }
                    else
                    {
                        msay.setEnabled(true);
                    }
                }
            }
        });
        medit=findViewById(R.id.edit);
        mpitch=findViewById(R.id.pi);
        msound=findViewById(R.id.sound);

        msay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                say();
            }
        });

    }
    private void say()
    {
        String s=medit.getText().toString();
        float p=(float) mpitch.getProgress()/50;
        if(p<0.1)p=0.1f;
        float so=(float) msound.getProgress()/50;
        if(so<0.1)p=0.1f;
        mtext.setPitch(p);
        mtext.setSpeechRate(so);
        mtext.speak(s,TextToSpeech.QUEUE_FLUSH,null);
    }
}
